package com.mapbox.simplemap.myapplication;

import java.io.Serializable;

public class GuardadosUsuario implements Serializable {

    private String nombre;
    private int foto;
    private String informacion;
    private int fotoDetalle;

    public GuardadosUsuario(GuardadosUsuario datos){

    }

    public GuardadosUsuario(String nombre,String informacion ,int foto, int fotoDetalle) {
        this.nombre = nombre;
        this.informacion = informacion;
        this.foto = foto;
        this.fotoDetalle = fotoDetalle;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public int getFotoDetalle() {
        return fotoDetalle;
    }

    public void setFotoDetalle(int fotoDetalle) {
        this.fotoDetalle = fotoDetalle;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

}
