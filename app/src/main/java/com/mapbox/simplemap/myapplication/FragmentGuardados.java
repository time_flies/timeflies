package com.mapbox.simplemap.myapplication;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentGuardados.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentGuardados#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGuardados extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View vista;
    RecyclerView recyclerView;
    ArrayList<ListaMapas> listaDatos;
    ImageView icono;
    SearchView sv1;

    private RequestQueue request;
    private StringRequest stringRequest;

    Activity activity;
    IComunicaFragments iComunicaFragments;

    private OnFragmentInteractionListener mListener;

    public FragmentGuardados() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentGuardados.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentGuardados newInstance(String param1, String param2) {
        FragmentGuardados fragment = new FragmentGuardados();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fragment_guardados, container, false);
        listaDatos = new ArrayList<>();
        listaDatos = VariablesGlobales.getListaGuardados();
        sv1 = vista.findViewById(R.id.sv1);

        request = Volley.newRequestQueue(getContext());

        recyclerView = vista.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (listaDatos.size()!= 0) {
            System.out.println("AQUI ESTAMOS 1");

            subirListaGuardados();
        }



        final AdapterDatosGuardados adapter = new AdapterDatosGuardados(listaDatos);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iComunicaFragments.enviarDato(listaDatos.get(recyclerView.getChildAdapterPosition(view)));
            }
        });



        sv1.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Toast.makeText(getActivity(),s,Toast.LENGTH_SHORT).show();
                adapter.setFilter(filtrado(listaDatos,s));
                return true;
            }
        });


        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) sv1.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));

        recyclerView.setAdapter(adapter);

        return vista;
    }


    private void subirListaGuardados() {
        String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=insertPlace";
        url = url.replace(" ","%20");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("ENVIANDO LISTA GUARDADOS:"+response);
                //TODO poner correctamente
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(new FragmentGuardados().getContext(),"Error en la comunicación."+error.toString(),Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                int idUsuario = VariablesGlobales.getId();
                String actividad;
                if (VariablesGlobales.getEleccion() != null) {
                    actividad = VariablesGlobales.getEleccion();
                }else{
                    actividad="ocio";
                }
                int i;

                for (i = 0; i < listaDatos.size()-1; i++) {

                    String nombreLugar = listaDatos.get(i).getNombre();
                    Double latitud = listaDatos.get(i).getLat();
                    Double longitud = listaDatos.get(i).getLng();
                    String descripcion = listaDatos.get(i).getCalle();
                    //String url = listaDatos.get(i).getUrl();
                    System.out.println(nombreLugar+latitud+longitud+descripcion);

                    Map<String, String> parametros = new HashMap<>();
                    parametros.put("idUsuario", String.valueOf(idUsuario));
                    parametros.put("nombreLugar", nombreLugar);
                    parametros.put("latitud", String.valueOf(latitud));
                    parametros.put("longitud", String.valueOf(longitud));
                    parametros.put("descripcion", descripcion);
                    parametros.put("actividad", actividad);
                    //parametros.put("photo",url);

                    return parametros;
                }

                String nombreLugar = listaDatos.get(i).getNombre();
                Double latitud = listaDatos.get(i).getLat();
                Double longitud = listaDatos.get(i).getLng();
                String descripcion = listaDatos.get(i).getCalle();
                //String url = listaDatos.get(i).getUrl();


                Map<String, String> parametros = new HashMap<>();
                parametros.put("idUsuario", String.valueOf(idUsuario));
                parametros.put("nombreLugar", nombreLugar);
                parametros.put("latitud", String.valueOf(latitud));
                parametros.put("longitud", String.valueOf(longitud));
                parametros.put("descripcion", descripcion);
                parametros.put("actividad", actividad);
                //parametros.put("photo",url);

                return parametros;
            }
        };
        request.add(stringRequest);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            this.activity = (Activity) context;
            iComunicaFragments = (IComunicaFragments) this.activity;
        }




        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private ArrayList<ListaMapas> filtrado(ArrayList<ListaMapas> listaDatos, String texto){
        ArrayList<ListaMapas> listaFiltrada = new ArrayList<>();

        texto = texto.toLowerCase();

        for (ListaMapas dato: listaDatos){
            String dato2 = dato.getNombre().toLowerCase();

            if (dato2.contains(texto)){
                listaFiltrada.add(dato);
            }
        }

        return listaFiltrada;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
