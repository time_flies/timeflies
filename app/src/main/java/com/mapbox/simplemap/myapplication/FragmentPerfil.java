package com.mapbox.simplemap.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentPerfil.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentPerfil#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPerfil extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RequestQueue request;
    private StringRequest stringRequest;

    private View vista;
    private EditText etNombre;
    private EditText etCorreo;
    private ImageView ivEditarNombre;
    private ImageView ivEditarCorreo;
    private ImageView fotoPerfil;
    private boolean editadoNombre;
    private boolean editadoCorreo;

    private ImageView ivPassword1;
    private ImageView ivPassword2;
    private EditText etPassword1;
    private EditText etPassword2;
    private Button buttonConfirmar;
    private Button buttonCambiar;

    private String nombre;
    private String email;
    private String antiguaContrasena;

    private OnFragmentInteractionListener mListener;

    public FragmentPerfil() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPerfil.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPerfil newInstance(String param1, String param2) {
        FragmentPerfil fragment = new FragmentPerfil();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fragment_perfil, container, false);

        request = Volley.newRequestQueue(getContext());

        etNombre = vista.findViewById(R.id.etNombre);
        etCorreo = vista.findViewById(R.id.etCorreo);
        ivEditarNombre = vista.findViewById(R.id.ivEditarNombre);
        ivEditarCorreo = vista.findViewById(R.id.ivEditarCorreo);
        fotoPerfil = vista.findViewById(R.id.ivPerfil);
        editadoNombre = false;
        editadoCorreo = false;

        nombre = VariablesGlobales.getUsuario();
        email = VariablesGlobales.getEmail();
        etNombre.setText(nombre);
        etCorreo.setText(email);

        ivPassword1 = vista.findViewById(R.id.ivPassword1);
        ivPassword2 = vista.findViewById(R.id.ivPassword2);
        etPassword2 = vista.findViewById(R.id.etPassword2);
        etPassword1 = vista.findViewById(R.id.etPassword1);
        buttonCambiar = vista.findViewById(R.id.buttonCambiar);
        buttonConfirmar = vista.findViewById(R.id.buttonConfirmar);

        ivPassword1.setVisibility(vista.INVISIBLE);
        ivPassword2.setVisibility(vista.INVISIBLE);
        etPassword1.setVisibility(vista.INVISIBLE);
        etPassword2.setVisibility(vista.INVISIBLE);
        buttonConfirmar.setVisibility(vista.INVISIBLE);

        if (VariablesGlobales.getFoto() != null) {
            cargarImagen();
        }else{
            fotoPerfil.setImageResource(R.drawable.perfil);
        }

        ivEditarNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNombre.setEnabled(true);
                if (!editadoNombre) {
                    ivEditarNombre.setImageResource(R.drawable.confirm);
                    editadoNombre = true;
                }else{
                    ivEditarNombre.setImageResource(R.drawable.edit);
                    editadoNombre = false;
                    etNombre.setEnabled(false);
                    String nombre = etNombre.getText().toString();
                    VariablesGlobales.setUsuario(nombre);
                    Toast.makeText(getActivity(),"Nombre: "+nombre,Toast.LENGTH_SHORT).show();
                    actualizarUsuario();
                }
            }
        });
        ivEditarCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCorreo.setEnabled(true);

                if (!editadoCorreo) {
                    ivEditarCorreo.setImageResource(R.drawable.confirm);
                    editadoCorreo = true;
                }else{
                    ivEditarCorreo.setImageResource(R.drawable.edit);
                    editadoCorreo = false;
                    etCorreo.setEnabled(false);
                    String correo = etCorreo.getText().toString();
                    VariablesGlobales.setEmail(correo);
                    Toast.makeText(getActivity(),"Email: "+correo,Toast.LENGTH_SHORT).show();
                    actualizarUsuario();
                }

            }
        });

        buttonCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCambiar.setVisibility(vista.INVISIBLE);
                ivPassword1.setVisibility(vista.VISIBLE);
                ivPassword2.setVisibility(vista.VISIBLE);
                etPassword1.setVisibility(vista.VISIBLE);
                etPassword2.setVisibility(vista.VISIBLE);
                buttonConfirmar.setVisibility(vista.VISIBLE);
            }
        });

        buttonConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCambiar.setVisibility(vista.VISIBLE);
                ivPassword1.setVisibility(vista.INVISIBLE);
                ivPassword2.setVisibility(vista.INVISIBLE);
                etPassword1.setVisibility(vista.INVISIBLE);
                etPassword2.setVisibility(vista.INVISIBLE);
                buttonConfirmar.setVisibility(vista.INVISIBLE);

                String oldpassword = etPassword1.getText().toString();
                String newpassword = etPassword2.getText().toString();
                antiguaContrasena = VariablesGlobales.getPassword();
                if (antiguaContrasena.equals(oldpassword)) {
                    if (newpassword.length()>0) {
                        Toast.makeText(getActivity(), "Antigua contraseña: " + oldpassword + "\nNueva contraseña: " + newpassword, Toast.LENGTH_SHORT).show();
                        VariablesGlobales.setPassword(newpassword);
                        actualizarUsuario();
                    }else{
                        Toast.makeText(getActivity(), "Error. La nueva contraseña no puede estar vacia.", Toast.LENGTH_SHORT).show();

                    }
                }else{
                    Toast.makeText(getActivity(), "Error. No se ha podido cambiar la contraseña", Toast.LENGTH_SHORT).show();
                }
                etPassword1.getText().clear();
                etPassword2.getText().clear();
            }
        });



        // Inflate the layout for this fragment
        return vista;
    }

    private void actualizarUsuario() {
        String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=updateUser";
        url = url.replace(" ","%20");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getContext(),"Envio de datos del usuario..."+response,Toast.LENGTH_SHORT).show();
                System.out.println("ACTUALIZACION USUARIO:"+response);
                //TODO poner correctamente
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Error en la comunicación."+error.toString(),Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String username = VariablesGlobales.getUsuario();
                String email = VariablesGlobales.getEmail();
                String password = VariablesGlobales.getPassword();
                int userId = VariablesGlobales.getId();

                Map<String,String> parametros = new HashMap<>();
                parametros.put("userId", String.valueOf(userId));
                parametros.put("username",username);
                parametros.put("email",email);
                parametros.put("password",password);
                //parametros.put("photo",nombreImagen);

                return parametros;
            }
        };
        request.add(stringRequest);
    }

    private void cargarImagen() {
        Toast.makeText(getActivity(),VariablesGlobales.getFoto(),Toast.LENGTH_LONG).show();

        String urlImagen = VariablesGlobales.getFoto();
        urlImagen = urlImagen.replace(" ","%20");
        ImageRequest imageRequest = new ImageRequest(urlImagen, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                fotoPerfil.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Error. No se ha podido cargar la imagen", Toast.LENGTH_SHORT).show();
            }
        });

        request.add(imageRequest);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
