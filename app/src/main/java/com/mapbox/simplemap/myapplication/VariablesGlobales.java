package com.mapbox.simplemap.myapplication;

import android.app.Application;

import java.util.ArrayList;

public class VariablesGlobales extends Application {

    private static int id;
    private static String usuario;
    private static String email;
    private static String password;
    private static String foto;

    private static ArrayList<ListaMapas> listaGuardados = new ArrayList<>();
    private static ArrayList<ListaMapas> listaLugares = new ArrayList<>();

    private static ArrayList<Aeropuerto> listaAeropuertos = new ArrayList<>();
    private static Aeropuerto aeropuerto;
    private static int numeroAeropuerto;
    private static String eleccion;

    public static String getUsuario(){return usuario; }
    public static void setUsuario(String user){
        usuario = user;
    }

    public static String getEmail(){
        return email;
    }
    public static void setEmail(String correo){ email = correo; }

    public static String getPassword(){ return password; }
    public static void setPassword(String psswrd){
        password = psswrd;
    }

    public static int getNumeroAeropuerto() { return numeroAeropuerto; }
    public static void setNumeroAeropuerto(int numeroAeropuerto) { VariablesGlobales.numeroAeropuerto = numeroAeropuerto;}

    public static String getEleccion() { return eleccion;}
    public static void setEleccion(String eleccion) { VariablesGlobales.eleccion = eleccion;}


    public static ArrayList<String> getNombresListaAeropuertos(){
        ArrayList<String> listaNombresAeropuertos = new ArrayList<>();
        for (int i = 0;i<listaAeropuertos.size();i++)
            listaNombresAeropuertos.add(listaAeropuertos.get(i).getNombre());
        return listaNombresAeropuertos;
    }
    public static ArrayList<Aeropuerto> getListaAeropuertos() { return listaAeropuertos; }
    public static void setListaAeropuertos(ArrayList<Aeropuerto> listaAeropuertos) {
        VariablesGlobales.listaAeropuertos = listaAeropuertos;
    }

    public static Aeropuerto getAeropuerto() { return aeropuerto; }

    public static void setAeropuerto(Aeropuerto aeropuerto) { VariablesGlobales.aeropuerto = aeropuerto; }

    public static int getId() { return id; }
    public static void setId(int id) { VariablesGlobales.id = id; }

    public static String getFoto() { return foto; }
    public static void setFoto(String foto) { VariablesGlobales.foto = foto; }

    public static ArrayList<ListaMapas> getListaGuardados() { return listaGuardados; }
    public static void setListaGuardados(ArrayList<ListaMapas> listaGuardados) { VariablesGlobales.listaGuardados = listaGuardados; }

    public static ArrayList<ListaMapas> getListaLugares() { return listaLugares; }
    public static void setListaLugares(ArrayList<ListaMapas> listaLugares) { VariablesGlobales.listaLugares = listaLugares; }
    public static void setListaLugares2(ListaMapas lugares) { VariablesGlobales.listaLugares.add(lugares); }

}
