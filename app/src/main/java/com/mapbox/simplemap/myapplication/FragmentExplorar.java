package com.mapbox.simplemap.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLOutput;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentExplorar.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentExplorar#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentExplorar extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject>{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View vista;
    SeekBar seekBar;
    CardView cardViewHoteles;
    CardView cardViewOcio;
    CardView cardViewRestaurantes;
    CardView cardViewMuseos;
    public String aeropuerto;
    ListaMapas lugares;
    ArrayList<ListaMapas> listaLugares;
    public RequestQueue request;
    int minutos;
    IComunicaFragments iComunicaFragments;
    Activity activity;

    ProgressDialog progressDialog;

    private OnFragmentInteractionListener mListener;

    public FragmentExplorar() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentExplorar.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentExplorar newInstance(String param1, String param2) {
        FragmentExplorar fragment = new FragmentExplorar();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fragment_explorar, container, false);

        request = Volley.newRequestQueue(getContext());
        listaLugares = new ArrayList<>();

        progressDialog = new ProgressDialog(getContext());

        seekBar = vista.findViewById(R.id.seekBar_explorar);
        cardViewOcio = vista.findViewById(R.id.cv1);
        cardViewRestaurantes = vista.findViewById(R.id.cv2);
        cardViewMuseos = vista.findViewById(R.id.cv3);
        cardViewHoteles = vista.findViewById(R.id.cv4);

        aeropuerto = VariablesGlobales.getListaAeropuertos().get(VariablesGlobales.getNumeroAeropuerto()).getNombre();
        seekBar.setMax(1440);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar1, int progress, boolean b) {
                Toast.makeText(getContext(), toHours(progress), Toast.LENGTH_LONG).show();
                minutos = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        cardViewOcio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aeropuerto == null) {
                    Toast.makeText(getContext(), "Ocio", Toast.LENGTH_SHORT).show();
                }else{
                    VariablesGlobales.setEleccion("ocio");
                    cargando();
                }
            }
        });

        cardViewRestaurantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aeropuerto == null) {
                    Toast.makeText(getContext(), "Restaurantes", Toast.LENGTH_SHORT).show();
                }else{
                    VariablesGlobales.setEleccion("restaurantes");
                    cargando();
                }
            }
        });

        cardViewMuseos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aeropuerto == null) {
                    Toast.makeText(getContext(), "Museos", Toast.LENGTH_SHORT).show();
                }else{
                    VariablesGlobales.setEleccion("museos");
                    cargando();


                }
            }
        });

        cardViewHoteles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aeropuerto == null) {
                    Toast.makeText(getContext(), "Hoteles", Toast.LENGTH_SHORT).show();
                }else{
                    VariablesGlobales.setEleccion("hoteles");
                    cargando();
                }
            }
        });

        return vista;
    }

    private void cargando() {
        progressDialog.setTitle(VariablesGlobales.getEleccion());
        progressDialog.setMessage("Cargando lugares que te puedan gustar...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        lecturaDatosGoogle();

    }

    private String toHours(int minutos) {
        if (minutos <= 60){
            return "tienes "+minutos+" minutos";
        }else{
            int horas = minutos/60;
            int min = minutos % 60;
            if (min == 0){
                return "Tienes "+horas+ " hora(s)";
            }else{
                return "Tienes "+horas+ " hora(s) y "+min+" minutos";
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            this.activity = (Activity) context;
            iComunicaFragments = (IComunicaFragments) this.activity;
        }


        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void lecturaDatosGoogle() {
        Double lat = VariablesGlobales.getAeropuerto().getLatitud();
        Double lng = VariablesGlobales.getAeropuerto().getLongitud();
        String tipo = VariablesGlobales.getEleccion();
        Double radio = calculaRadio();
        int radius = radio.intValue();

        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+lng+"&radius="+radius+"&type="+tipo+"&key=AIzaSyAHyr-B3lJYEx-vS5OLpAMEnpMldmdruWU";
        url = url.replace(" ","%20");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);

        request.add(jsonObjectRequest);
    }

    private Double calculaRadio() {
        Double tiempoMinutosTotal = Double.valueOf(minutos);

        return (tiempoMinutosTotal*833.333333333)/2;
    }

    @Override
    public void onResponse(JSONObject response) {

        String photo_reference = null;
        String nombre = null;
        Integer rating = null;
        String calle = null;
        Double lat =null;
        Double lng =null;
        Boolean abierto = true;
        Bitmap photo = null;
        String url = null;


        JSONArray json = response.optJSONArray("results");

        for (int i = 0; i < json.length(); i++) {

            try {
                JSONObject jsonObject = json.getJSONObject(i);
                nombre = jsonObject.optString("name");
                rating = jsonObject.optInt("rating");
                calle = jsonObject.optString("vicinity");

                JSONObject jsonObjectGeometry = jsonObject.getJSONObject("geometry");
                JSONObject jsonObjectGeometryLocation = jsonObjectGeometry.getJSONObject("location");
                lat = jsonObjectGeometryLocation.getDouble("lat");
                lng = jsonObjectGeometryLocation.getDouble("lng");
/*
                JSONObject jsonObjectOpeningHours = jsonObject.getJSONObject("opening_hours");
                abierto = jsonObjectOpeningHours.getBoolean("open_now");
*/
                JSONArray jsonArrayPhotos = jsonObject.getJSONArray("photos");
                //for (int j = 0; j < jsonArrayPhotos.length() ; j++) {
                    JSONObject jsonObjectPhotos = jsonArrayPhotos.getJSONObject(0);
                    photo_reference = jsonObjectPhotos.getString("photo_reference");
                    url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&maxheight=900&photoreference="+photo_reference+" &key=AIzaSyAHyr-B3lJYEx-vS5OLpAMEnpMldmdruWU";
                    url = url.replace(" ","%20");
                //}

            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println(photo_reference);

            lugares = new ListaMapas(nombre,calle,rating,abierto,lat,lng,url);

            listaLugares.add(lugares);

        }
        VariablesGlobales.getListaLugares().clear();
        VariablesGlobales.setListaLugares(listaLugares);
        iComunicaFragments.seleccionadoCosa();
        progressDialog.dismiss();

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getContext(),"Error en la comunicacion con Google",Toast.LENGTH_LONG).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
