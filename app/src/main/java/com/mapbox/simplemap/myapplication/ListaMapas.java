package com.mapbox.simplemap.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class ListaMapas implements Serializable {

    private String nombre;
    private String calle;
    private int rating;
    private boolean abierto;
    private Double lat;
    private Double lng;
    private int imagen;
    private Bitmap photo;
    private String url;
    private RequestQueue request;

    public ListaMapas(ListaMapas listaMapas) {

    }

    public ListaMapas(String nombre, String calle, Integer rating, Boolean abierto, Double lat, Double lng, String url){
        System.out.println(url);
        this.nombre = nombre;
        this.calle = calle;
        this.rating = rating;
        this.abierto = abierto;
        this.lat = lat;
        this.lng = lng;
        this.url = url;
        this.photo = getBitmapFromURL(url);

    }

    public ListaMapas(String nombre, String calle, Double lat, Double lng){
        System.out.println(url);
        this.nombre = nombre;
        this.calle = calle;
        this.lat = lat;
        this.lng = lng;

    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalle() {
        return calle;
    }
    public void setCalle(String calle) {
        this.calle= calle;
    }

    public int getImagen() {
        return imagen;
    }
    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public int getRating() { return rating; }
    public void setRating(int rating) { this.rating = rating; }

    public boolean isAbierto() {return abierto; }
    public void setAbierto(boolean abierto) {this.abierto = abierto; }

    public Double getLat() {return lat; }
    public void setLat(Double lat) { this.lat = lat; }

    public Double getLng() { return lng; }
    public void setLng(Double lng) { this.lng = lng; }

    public Bitmap getPhoto() { return photo; }
    public void setPhoto(Bitmap photo) {this.photo = photo;}

    public String getUrl() { return url; }
    public void setUrl(String url) { this.url = url;}


    public static Bitmap getBitmapFromURL(String src) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            System.out.println("DEVUELVO LA PUTA FOTO");
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


}
