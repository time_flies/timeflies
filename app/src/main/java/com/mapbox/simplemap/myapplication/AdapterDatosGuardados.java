package com.mapbox.simplemap.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterDatosGuardados extends RecyclerView.Adapter<AdapterDatosGuardados.ViewHolderDatos>
                                    implements View.OnClickListener , Filterable {

    ArrayList<ListaMapas> listaDatos;
    private View.OnClickListener listener;

    public AdapterDatosGuardados(ArrayList<ListaMapas> listaDatos) {
        this.listaDatos = listaDatos;
    }


    @Override
    public ViewHolderDatos onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_guardados,null,false);

        view.setOnClickListener(this);

        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int i) {
        System.out.println("\n\n\nPRUEBA: "+listaDatos.get(i).getNombre());

        holder.etNombre.setText(listaDatos.get(i).getNombre());
        //holder.foto.setImageResource(listaDatos.get(i).getImagen());
        holder.foto.setImageBitmap(listaDatos.get(i).getPhoto());
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null){
            listener.onClick(view);
        }
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView etNombre;
        ImageView foto;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            etNombre = itemView.findViewById(R.id.tvNombre);
            foto = itemView.findViewById(R.id.ivFoto);
        }
    }

    public void setFilter(ArrayList<ListaMapas> listaDatos2){
        listaDatos = listaDatos2;
        notifyDataSetChanged();
    }

}
