package com.mapbox.simplemap.myapplication;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdapterDatosMaps extends RecyclerView.Adapter<AdapterDatosMaps.ViewHolderDatos>
                                implements View.OnClickListener{

    ArrayList<ListaMapas> listDatos;
    ListaMapas lugarGuardado;
    private View.OnClickListener listener;
    LikeButton likeButton;
    int pulsado = -1;
    FragmentMaps fragmentMaps;
    //RequestQueue request;

    public AdapterDatosMaps(ArrayList<ListaMapas> listDatos) {
        this.listDatos = listDatos;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View vista = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista_mapas,null,false);

        vista.setOnClickListener(this);

        return new ViewHolderDatos(vista);
    }

    @Override
    public void onBindViewHolder(final ViewHolderDatos holder,final int i) {
        holder.tvNombre.setText(listDatos.get(i).getNombre());
        holder.imageView.setImageBitmap(listDatos.get(i).getPhoto());

        if (VariablesGlobales.getUsuario().length()==0){
            likeButton.setVisibility(View.INVISIBLE);
        }else {
            for (int j = 0; j<getItemCount();j++){
                if (listDatos.get(i).getNombre().equals("Prueba 1"))
                    likeButton.setLiked(true);
            }
        }
        likeButton.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                VariablesGlobales.getListaGuardados().add(listDatos.get(i));
                System.out.println("añadido me gusta");
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                VariablesGlobales.getListaGuardados().remove(listDatos.get(i));
                System.out.println("eliminado me gusta");
            }
        });

    }
/*
    private void subirListaGuardados(final ListaMapas listaGuardados) {
        String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=insertPlace";
        url = url.replace(" ","%20");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(new FragmentGuardados().getContext(),"Envio de datos del usuario..."+response,Toast.LENGTH_SHORT).show();
                System.out.println("ACTUALIZACION USUARIO:"+response);
                //TODO poner correctamente
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(new FragmentGuardados().getContext(),"Error en la comunicación."+error.toString(),Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                int idUsuario = VariablesGlobales.getId();
                String nombreLugar = listaGuardados.getNombre();
                Double latitud = listaGuardados.getLat();
                Double longitud = listaGuardados.getLng();
                String descripcion = listaGuardados.getCalle();
                String actividad = VariablesGlobales.getEleccion();

                Map<String,String> parametros = new HashMap<>();
                parametros.put("idUsuario", String.valueOf(idUsuario));
                parametros.put("nombreLugar",nombreLugar);
                parametros.put("latitud",String.valueOf(latitud));
                parametros.put("longitud",String.valueOf(longitud));
                parametros.put("descripcion",descripcion);
                parametros.put("actividad",actividad);
                //parametros.put("photo",nombreImagen);

                return parametros;
            }
        };
        request.add(stringRequest);
    }
*/
    @Override
    public int getItemCount() {
        return listDatos.size();
    }


    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null){
            listener.onClick(view);
        }
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView tvNombre;
        ImageView imageView;
        //LikeButton likeButton;
        ConstraintLayout constraintLayout;


        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            tvNombre = itemView.findViewById(R.id.tvNombreMaps);
            imageView = itemView.findViewById(R.id.ivMaps);
            likeButton = itemView.findViewById(R.id.heart_button);
            constraintLayout = itemView.findViewById(R.id.contraintLayout);
            //request = Volley.newRequestQueue(new FragmentPerfil().getActivity());
        }
    }
}
