package com.mapbox.simplemap.myapplication;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMaps.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMaps# newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMaps extends Fragment implements OnMapReadyCallback{

    GoogleMap mGoogleMap;
    MapView mMapView;
    View vista;
    TextView titulo;
    LikeButton likeButton;

    RecyclerView recyclerView;
    ImageView mostrarOcultar;
    boolean mostrar;
    boolean seleccionado;

    RecyclerView.ViewHolder pulsado;

    public FragmentMaps() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fragment_maps, container, false);

        titulo = vista.findViewById(R.id.tvTituloLista);
        mostrarOcultar = vista.findViewById(R.id.ivmostrarocultar);
        recyclerView = vista.findViewById(R.id.recyclerviewMaps);
        mostrar = false;
        seleccionado = false;

        System.out.println("CARGANDO LA LISTA DE LUGARES.");
        final AdapterDatosMaps adapter = new AdapterDatosMaps(VariablesGlobales.getListaLugares());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getContext(),"Seleccionado: "+VariablesGlobales.getListaLugares().get(recyclerView.getChildAdapterPosition(view)).getNombre(),Toast.LENGTH_SHORT).show();

                if(!seleccionado){
                    recyclerView.getChildViewHolder(view).itemView.setBackgroundColor(Color.parseColor("#A8C6FA"));
                    seleccionado = true;

                }else{
                    pulsado.itemView.setBackgroundColor(Color.TRANSPARENT);
                    recyclerView.getChildViewHolder(view).itemView.setBackgroundColor(Color.parseColor("#A8C6FA"));
                }
                pulsado = recyclerView.getChildViewHolder(view);

            }

        });

        recyclerView.setAdapter(adapter);

        if (VariablesGlobales.getAeropuerto()!= null && VariablesGlobales.getEleccion() != null) {
            titulo.setText("Lista cercana a " + VariablesGlobales.getAeropuerto().getNombre() + " de " + VariablesGlobales.getEleccion());
        }else{
            titulo.setText("Lista de recomendados");
        }



        recyclerView.setVisibility(vista.INVISIBLE);

        mostrarOcultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mostrar){
                    recyclerView.setVisibility(vista.VISIBLE);
                    mostrarOcultar.setImageResource(R.drawable.ocultarlista);
                    mostrar = true;
                }else{
                    recyclerView.setVisibility(vista.INVISIBLE);
                    mostrarOcultar.setImageResource(R.drawable.mostrarlista);
                    mostrar = false;
                }

            }
        });


        return vista;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = vista.findViewById(R.id.map);
        if (mMapView!=null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());

        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        for(ListaMapas lugar : VariablesGlobales.getListaLugares()) {
            añadirMarca(googleMap, lugar.getLat(), lugar.getLng(), lugar.getNombre(), lugar.getCalle());
        }

        CameraPosition laCasa = posicionInicialCamara();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(laCasa));

    }

    private void añadirMarca(GoogleMap googleMap, Double lat, Double lng, String nombre, String calle) {

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat,lng))
                .title(nombre)
                .snippet(calle)
        );
    }

    private CameraPosition posicionInicialCamara() {
        Double lat = 42.8168700;
        Double lng = -1.6432300;
        if (VariablesGlobales.getAeropuerto() != null) {
            lat = VariablesGlobales.getAeropuerto().getLatitud();
            lng = VariablesGlobales.getAeropuerto().getLongitud();
        }

        CameraPosition laCasa = CameraPosition.builder()
                .target(new LatLng(lat,lng))
                .zoom(10)
                .bearing(0)
                .tilt(45)
                .build();
        return laCasa;

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
