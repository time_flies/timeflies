package com.mapbox.simplemap.myapplication;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.InterstitialAd;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAeropuertos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAeropuertos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAeropuertos extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SearchView sv1;
    private ListView lv1;
    //private ArrayList<String> listaAeropuertos;
    View vista;
    IComunicaFragments iComunicaFragments;
    Activity activity;
    ArrayList<ListaMapas> listaDatos;

    RequestQueue request;
    private StringRequest stringRequestGuardados;
    private InterstitialAd mInterstitialAd;


    private OnFragmentInteractionListener mListener;

    public FragmentAeropuertos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAeropuertos.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAeropuertos newInstance(String param1, String param2) {
        FragmentAeropuertos fragment = new FragmentAeropuertos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fragment_aeropuertos, container, false);
        sv1=vista.findViewById(R.id.sv1);
        lv1=vista.getRootView().findViewById(R.id.lv1);
        request = Volley.newRequestQueue(getContext());
        listaDatos = new ArrayList<>();


        //MobileAds.initialize(getContext(),"ca-app-pub-9187011433652886~6088377650")
        //MobileAds.initialize(getContext(),"ca-app-pub-3940256099942544~1033173712");

        mInterstitialAd = new InterstitialAd(getContext());
        //mInterstitialAd.setAdUnitId("ca-app-pub-9187011433652886/6088377650");
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");


        AdRequest adRequest = new AdRequest.Builder().build();

        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });

        //leerListaGuardados();



        final ArrayAdapter adaptador = new ArrayAdapter(getActivity().getApplicationContext(),android.R.layout.simple_list_item_1,VariablesGlobales.getNombresListaAeropuertos());
        lv1.setAdapter(adaptador);


        sv1.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String busqueda) {
                adaptador.getFilter().filter(busqueda);
                Toast.makeText(getActivity(),busqueda,Toast.LENGTH_SHORT).show();
                return true;
            }
        });


        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String aeropuerto = adapterView.getItemAtPosition(i).toString();

                VariablesGlobales.setAeropuerto(VariablesGlobales.getListaAeropuertos().get(i));

                Toast.makeText(getActivity(),"Seleccionado: "+
                                VariablesGlobales.getAeropuerto().getNombre()
                                + "\n latitud: "+VariablesGlobales.getAeropuerto().getLatitud()
                                + "\n lng: "+VariablesGlobales.getAeropuerto().getLongitud()

                        ,Toast.LENGTH_SHORT).show();

                iComunicaFragments.seleccionadoAeropuerto();
            }
        });

        return vista;
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()){
            mInterstitialAd.show();
        }

    }


    private void leerListaGuardados() {
        String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=getProfileByUserId";
        url = url.replace(" ","%20");

        stringRequestGuardados = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                System.out.println("LISTA GUARDADOS:"+response);

                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.optJSONArray("data");


                for (int i = 0; i<jsonArray.length();i++){
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    JSONArray jsonArray1 = jsonObject1.optJSONArray("data");


                    JSONObject jsonObject2 = jsonArray1.getJSONObject(0);
                    String nombreLugar = jsonObject2.optString("nombreLugar");
                    Double latitud = jsonObject2.optDouble("latitud");
                    Double longitud = jsonObject2.optDouble("longitud");
                    String descripcion = jsonObject2.optString("descripcion");
                    String actividad = jsonObject2.optString("actividad");
                    System.out.println(nombreLugar+latitud);
                    ListaMapas lugarGuardado = new ListaMapas(nombreLugar, descripcion ,latitud, longitud);
                    listaDatos.add(lugarGuardado);
                    //VariablesGlobales.setEleccion(actividad);
                }
                VariablesGlobales.setListaGuardados(listaDatos);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Error en la obtención de listas de guardados.",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                int userId = VariablesGlobales.getId();
                Map<String,String> parametros = new HashMap<>();
                parametros.put("userId", String.valueOf(userId));

                return parametros;
            }
        };

        request.add(stringRequestGuardados);

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            this.activity = (Activity) context;
            iComunicaFragments = (IComunicaFragments) this.activity;
        }

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
