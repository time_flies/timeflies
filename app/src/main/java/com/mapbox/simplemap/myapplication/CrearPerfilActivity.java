package com.mapbox.simplemap.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class CrearPerfilActivity extends AppCompatActivity {

    private EditText et1;
    private EditText et2;
    private EditText et3;
    private EditText et4;
    private TextView tv1;
    private TextView tv2;

    private final String CARPETA_RAIZ="misImagenes/";
    private final String RUTA_IMAGEN= CARPETA_RAIZ+"misFotos";
    private ImageView imagen;
    private Button button;
    private String path;
    private String nombreImagen;

    Bitmap bitmap;

    final int COD_SELECCIONA = 10;
    final int COD_FOTO = 20;

    private RequestQueue request;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crearperfil);

        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);
        imagen = findViewById(R.id.ivPerfil);
        button = findViewById(R.id.button2);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        request = Volley.newRequestQueue(this);

        tv1.setPaintFlags(tv1.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        tv2.setPaintFlags(tv2.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        if (validaPermisos()){
            button.setEnabled(true);
        }else{
            button.setEnabled(false);
        }
    }

    private boolean validaPermisos() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            return true;
        }

        if ((checkSelfPermission(CAMERA)== PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)) {
            return true;
        }

        if ((shouldShowRequestPermissionRationale(CAMERA))||(shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            cargarDialogoRecomendacion();
        }else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==100){
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                button.setEnabled(true);
            }else {
                solicitarPermisosManual();
            }
        }
    }

    private void solicitarPermisosManual() {
        final CharSequence[] opciones = {"si","no"};
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(CrearPerfilActivity.this);
        alertOpciones.setTitle("¿Desea configurar los permisos de forma manual?");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("si")){
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package",getPackageName(),null);
                    intent.setData(uri);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Los permisos no fueron aceptados.",Toast.LENGTH_SHORT).show();
                    dialogInterface.dismiss();
                }
            }
        });
        alertOpciones.show();

    }

    private void cargarDialogoRecomendacion() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(CrearPerfilActivity.this);
        dialogo.setTitle("Permisos desactivados");
        dialogo.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");

        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        });
        dialogo.show();
    }

    public void cargarImagen(View view) {

        final CharSequence[] opciones = {"Camara","Galeria"};
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(CrearPerfilActivity.this);
        alertOpciones.setTitle("Seleccione una opción");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("Camara")){
                    tomarFoto();
                }else if (opciones[i].equals("Galeria")){
                    Intent intent= new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/");
                    startActivityForResult(intent.createChooser(intent,"Seleccione la Aplicación"),COD_SELECCIONA);
                }else{
                    dialogInterface.dismiss();
                }
            }
        });
        alertOpciones.show();
    }

    private void tomarFoto() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        nombreImagen = "";
        File fileImagen= new File(Environment.getExternalStorageDirectory(),RUTA_IMAGEN);
        boolean isCreada = fileImagen.exists();

        if (isCreada==false){
            isCreada = fileImagen.mkdirs();
        }else{
            nombreImagen=(System.currentTimeMillis()/100)+".jpg";
        }

        path=Environment.getExternalStorageDirectory()+File.separator+RUTA_IMAGEN+File.separator+nombreImagen;

        File imagen = new File(path);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        startActivityForResult(intent,COD_FOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){

            switch (requestCode){
                case COD_SELECCIONA:
                    Uri miPath=data.getData();
                    imagen.setImageURI(miPath);


                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(),miPath);
                        imagen.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String[] partesNombre = miPath.toString().replace("%2F","/").split("/");
                    System.out.println("COD_SELECCIONA: "+partesNombre[partesNombre.length - 1]);
                    nombreImagen = partesNombre[partesNombre.length - 1];


                    break;

                case COD_FOTO:
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    MediaScannerConnection.scanFile(this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String s, Uri uri) {
                            Log.i("Ruta de almacenamiento","Path: "+path);
                        }
                    });
                    bitmap = BitmapFactory.decodeFile(path);
                    imagen.setImageBitmap(bitmap);
                    String[] partesNombres = path.split("/");
                    System.out.println("COD_FOTO: "+partesNombres[partesNombres.length - 1]);
                    nombreImagen = partesNombres[partesNombres.length - 1];
                    break;

            }

        }
    }

    public void crearUsuario(View view) {
        final String nombre = et1.getText().toString();
        final String email = et2.getText().toString();
        final String psswrd1 = et3.getText().toString();
        String psswrd2 = et4.getText().toString();

        if (psswrd1.length() == 0 || psswrd2.length() == 0) {
            et3.getText().clear();
            et4.getText().clear();

            Toast notificacion = Toast.makeText(this, "la contraseña no puede estar vacia", Toast.LENGTH_LONG);
            notificacion.show();
        } else {
            if (nombre.length() == 0){
                Toast notificacion = Toast.makeText(this, "el nombre no puede estar vacio", Toast.LENGTH_LONG);
                notificacion.show();
            }else if (email.length() == 0){
                Toast notificacion = Toast.makeText(this, "el email no puede estar vacio", Toast.LENGTH_LONG);
                notificacion.show();
            }else if (!psswrd1.equals(psswrd2)) {
                et3.getText().clear();
                et4.getText().clear();

                Toast notificacion = Toast.makeText(this, "las contraseñas no coinciden", Toast.LENGTH_LONG);
                notificacion.show();
            } else {
                System.out.println("usuario: " + nombre + "\nemail:" + email + "\ncontraseña1:" + psswrd1 + "\ncontraseña2:" + psswrd2);

                String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=registerUser";
                url = url.replace(" ","%20");

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplication(),"Envio de datos del usuario..."+response,Toast.LENGTH_SHORT).show();
                        System.out.println("ENVIO DE DATOS AL USUARIO: NOMBRE"+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (!jsonObject.optString("nombreUsuario").equals("noRegistrado") && !jsonObject.optString("nombreUsuario").equals("yaRegistrado")) {

                                Toast.makeText(getApplication(),"Usuario Registrado correctamente.",Toast.LENGTH_LONG).show();

                                VariablesGlobales.setId(jsonObject.optInt("idUsuario"));
                                VariablesGlobales.setUsuario(jsonObject.optString("nombreUsuario"));
                                VariablesGlobales.setPassword(jsonObject.optString("contrasena"));
                                VariablesGlobales.setEmail(jsonObject.optString("email"));
                                VariablesGlobales.setFoto("http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS" + jsonObject.optString("pathFoto"));
                                /*
                                VariablesGlobales.setEmail(email);
                                VariablesGlobales.setPassword(psswrd1);
                                VariablesGlobales.setUsuario(nombre);
                                VariablesGlobales.setFoto(nombreImagen);
                                */

                                accesoCorrecto();

                            }else if(jsonObject.optString("nombreUsuario").equals("noRegistrado")){
                                Toast.makeText(getApplication(),"Error en usuario o contraseña.",Toast.LENGTH_LONG).show();
                                et1.getText().clear();
                                et2.getText().clear();
                                et3.getText().clear();
                                et4.getText().clear();
                            }else if(jsonObject.optString("nombreUsuario").equals("yaRegistrado")){
                                Toast.makeText(getApplication(),"Usuario o email ya existen.",Toast.LENGTH_LONG).show();
                                //TODO ponerlo bien.
                                et1.getText().clear();
                                et2.getText().clear();
                                et3.getText().clear();
                                et4.getText().clear();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplication(),"Error en la comunicación."+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> parametros = new HashMap<>();

                        if (bitmap != null) {
                            String imagen = convertirImgString(bitmap);
                            parametros.put("photo",imagen);
                        }

                        parametros.put("username",nombre);
                        parametros.put("email",email);
                        parametros.put("password",psswrd1);


                        return parametros;
                    }
                };

                request.add(stringRequest);

                //Toast.makeText(this,((VariablesGlobales)this.getApplication()).getFoto(),Toast.LENGTH_LONG).show();


            }
        }
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,50,array);
        byte[] imagenByte = array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);

        return imagenString;
    }

    private void accesoCorrecto() {


        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void entrarUsuarioInvitado(View view) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void login(View view) {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}




