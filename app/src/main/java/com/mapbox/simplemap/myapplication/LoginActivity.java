package com.mapbox.simplemap.myapplication;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LoginActivity extends AppCompatActivity{
    private EditText etUser;
    private EditText etPassword;

    private RequestQueue request;
    private StringRequest stringRequest;
    private StringRequest stringRequestAeropuertos;
    private StringRequest stringRequestGuardados;
    private ArrayList<Aeropuerto> listaAeropuertos;
    private Aeropuerto aeropuerto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUser = findViewById(R.id.et1);
        etPassword = findViewById(R.id.et2);
        listaAeropuertos = new ArrayList<>();

        request = Volley.newRequestQueue(this);

        leerAeropuertos();

    }

    private void leerAeropuertos() {
        String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=getAirports";
        url = url.replace(" ","%20");

        stringRequestAeropuertos = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Toast.makeText(getApplication(),"leyendo aeropuertos...\n",Toast.LENGTH_LONG).show();
                try {
                    System.out.println(response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.optJSONArray("data");

                    for (int i = 0; i<jsonArray.length();i++){
                        JSONObject jsonObject1 = null;
                        jsonObject1 = jsonArray.getJSONObject(i);

                        aeropuerto = new Aeropuerto(jsonObject1.optString("nombreAeropuerto"),jsonObject1.optDouble("latitud"),jsonObject1.optDouble("longitud"));

                        listaAeropuertos.add(aeropuerto);
                    }

                    VariablesGlobales.setListaAeropuertos(listaAeropuertos);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplication(),"Error en la obtención de los aeropuertos.",Toast.LENGTH_LONG).show();
            }
        });

        request.add(stringRequestAeropuertos);


    }

    public void enviarCorreo(View view){
        String user = etUser.getText().toString();
        Toast.makeText(this,"enviar contraseña a: "+user,Toast.LENGTH_SHORT).show();
    }

    public void activity_CrearCuenta(View view){
        Intent intent = new Intent(this, CrearPerfilActivity.class);
        startActivity(intent);
    }
    public void entrar(View view){
        String url = "http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS/controller/endpointController.php?endpoint=getUser";
        final String user = etUser.getText().toString();
        final String password = etPassword.getText().toString();

        url = url.replace(" ","%20");

        if (user.length()==0 || password.length()==0){
            Toast.makeText(this,"No puede dejar campos vacios.",Toast.LENGTH_LONG).show();
        }else{

           stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
               @Override
               public void onResponse(String response) {
                   //Toast.makeText(getApplication(),"Envio de datos del usuario..."+response,Toast.LENGTH_SHORT).show();

                   JSONObject jsonObject = null;

                   try {
                       jsonObject = new JSONObject(response);

                       if (!jsonObject.optString("nombreUsuario").equals("noRegistrado")) {

                           VariablesGlobales.setId(jsonObject.optInt("idUsuario"));
                           VariablesGlobales.setUsuario(jsonObject.optString("nombreUsuario"));
                           VariablesGlobales.setPassword(jsonObject.optString("contrasena"));
                           VariablesGlobales.setEmail(jsonObject.optString("email"));
                           VariablesGlobales.setFoto("http://ec2-18-218-161-37.us-east-2.compute.amazonaws.com/WS" + jsonObject.optString("pathFoto"));

                           accesoCorrecto();
                       }else{
                           Toast.makeText(getApplication(),"Error en usuario o contraseña.",Toast.LENGTH_LONG).show();
                       }

                   } catch (JSONException e) {
                       e.printStackTrace();
                   }



               }
           }, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {
                   Toast.makeText(getApplication(),"Error en la comunicación."+error.toString(),Toast.LENGTH_SHORT).show();
               }
           }){
               @Override
               protected Map<String, String> getParams() throws AuthFailureError {
                   Map<String,String> parametros = new HashMap<>();
                   parametros.put("username",user);
                   parametros.put("password",password);

                   return parametros;
               }
           };

           request.add(stringRequest);

        }
    }

    private void accesoCorrecto() {
        //Toast.makeText(this,"Envio de datos del usuario...\nUsuario:"+((VariablesGlobales)this.getApplication()).getUsuario()+"\nPassword:"+((VariablesGlobales)this.getApplication()).getPassword(),Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void entrarUsuarioInvitado(View view){
        ((VariablesGlobales)this.getApplication()).setUsuario("");
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        //Toast.makeText(this,"Usuario:"+((VariablesGlobales)this.getApplication()).getUsuario(),Toast.LENGTH_SHORT).show();
    }



}
