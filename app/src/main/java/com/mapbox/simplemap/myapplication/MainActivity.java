package com.mapbox.simplemap.myapplication;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements FragmentAeropuertos.OnFragmentInteractionListener, FragmentPerfil.OnFragmentInteractionListener,
                                                            FragmentConstruccion.OnFragmentInteractionListener, FragmentGuardadosInvitado.OnFragmentInteractionListener,
                                                    FragmentPerfilInvitado.OnFragmentInteractionListener, FragmentExplorar.OnFragmentInteractionListener,
                                                    FragmentGuardados.OnFragmentInteractionListener, FragmentDetalleGuardadosUsuario.OnFragmentInteractionListener,
                                                    FragmentMaps.OnFragmentInteractionListener,IComunicaFragments {
    FragmentAeropuertos fragmentaeropuertos;
    FragmentPerfil fragmentPerfil;
    FragmentConstruccion fragmentConstruccion;
    FragmentGuardadosInvitado fragmentGuardadosInvitado;
    FragmentPerfilInvitado fragmentPerfilInvitado;
    FragmentExplorar fragmentExplorar;
    FragmentGuardados fragmentGuardados;
    FragmentDetalleGuardadosUsuario fragmentDetalleGuardadosUsuario;
    FragmentMaps fragmentMaps;


    String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentaeropuertos = new FragmentAeropuertos();
        fragmentPerfil = new FragmentPerfil();
        fragmentConstruccion = new FragmentConstruccion();
        fragmentGuardadosInvitado = new FragmentGuardadosInvitado();
        fragmentPerfilInvitado = new FragmentPerfilInvitado();
        fragmentExplorar = new FragmentExplorar();
        fragmentGuardados = new FragmentGuardados();
        fragmentMaps = new FragmentMaps();

        usuario = ((VariablesGlobales)this.getApplication()).getUsuario();

        getSupportFragmentManager().beginTransaction().add(R.id.contenedorFragments,fragmentaeropuertos).commit();

        BottomNavigationView myBottonNavigation = findViewById(R.id.bottom_navigation);
        myBottonNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.prueba1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentaeropuertos).addToBackStack(null).commit();
                        break;
                    case R.id.prueba2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentExplorar).addToBackStack(null).commit();
                        break;
                    case R.id.prueba3:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentMaps).addToBackStack(null).commit();
                        break;
                    case R.id.prueba4:
                        if (usuario.length()>0) {
                            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentGuardados).addToBackStack(null).commit();
                        }else{
                            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentGuardadosInvitado).addToBackStack(null).commit();
                        }
                        break;
                    case R.id.prueba5:
                        if (usuario.length()>0)
                            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentPerfil).addToBackStack(null).commit();
                        else
                            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentPerfilInvitado).addToBackStack(null).commit();
                        break;

                }
                return true;
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void enviarDato(ListaMapas guardadosUsuario) {
        fragmentDetalleGuardadosUsuario = new FragmentDetalleGuardadosUsuario();
        Bundle bundleEnvio = new Bundle();
        bundleEnvio.putSerializable("objeto",guardadosUsuario);
        fragmentDetalleGuardadosUsuario.setArguments(bundleEnvio);

        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentDetalleGuardadosUsuario).addToBackStack(null).commit();
    }

    public void seleccionadoAeropuerto(){
        BottomNavigationView myBottonNavigation = findViewById(R.id.bottom_navigation);
        myBottonNavigation.setSelectedItemId(R.id.prueba2);
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentExplorar).addToBackStack(null).commit();
    }

    public void seleccionadoCosa(){
        BottomNavigationView myBottonNavigation = findViewById(R.id.bottom_navigation);
        myBottonNavigation.setSelectedItemId(R.id.prueba3);
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments,fragmentMaps).addToBackStack(null).commit();
    }
}
